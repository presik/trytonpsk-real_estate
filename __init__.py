# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import Pool

# import kind_contract
from . import (
    configuration,
    contract,
    invoice,
    lessor_contract,
    property,
    sale,
    voucher,
)


def register():
    Pool.register(
        configuration.Configuration,
        # account.AccountConfiguration,
        property.PropertyCheckList,
        property.KindProperty,
        property.ItemCheckList,
        property.ProprietaryParty,
        property.DebtorsParty,
        property.Property,
        property.CreateLeaseholderContractStart,
        property.CreateLessorContractStart,
        property.PropertyReportStart,
        contract.SaleContract,
        contract.SaleContractLine,
        lessor_contract.LessorContract,
        lessor_contract.LessorContractSale,
        lessor_contract.CreateInvoicesProprietaryStart,
        invoice.Invoice,
        invoice.InvoiceLine,
        sale.Sale,
        voucher.Voucher,
        voucher.VoucherLine,
        module='real_estate', type_='model')
    Pool.register(
        property.CreateLeaseholderContract,
        property.CreateLessorContract,
        property.PropertyReportWizard,
        lessor_contract.CreateInvoicesProprietary,
        lessor_contract.CreateInvoiceLessor,
        module='real_estate', type_='wizard')
    Pool.register(
        lessor_contract.LessorContractReport,
        contract.RealEstateSaleContractReport,
        property.FinishContractReport,
        property.PropertyReport,
        property.RealEstateDunningReport,
        module='real_estate', type_='report')

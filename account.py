# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta


class AccountConfiguration(metaclass=PoolMeta):
    __name__ = 'account.configuration'

    default_observation_invoice = fields.Text('Default Observation for Invoices')

# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.i18n import gettext
from trytond.model import ModelSingleton, ModelSQL, ModelView, fields
from trytond.model.exceptions import AccessError
from trytond.pyson import Eval, Id, If
from trytond.transaction import Transaction


class Configuration(ModelSingleton, ModelSQL, ModelView):
    "Configuration"
    __name__ = 'real_estate.configuration'
    company = fields.Many2One('company.company', 'Company', required=True,
        domain=[
            ('id', If(Eval('context', {}).contains('company'), '=', '!='),
                Eval('context', {}).get('company', 0)),
        ])
    ipc_value = fields.Numeric('IPC Value', digits=(4, 2))
    ipc_plus = fields.Numeric('IPC Plus', digits=(4, 2))
    legal_information = fields.Text('Legal Information')
    property_sequence = fields.Many2One('ir.sequence',
        'Property Sequence', domain=[
            ('company', 'in', [Eval('context', {}).get('company', 0), None]),
            ('sequence_type', '=', Id('real_estate', 'sequence_type_real_estate')),
        ], required=True)
    lessor_contract_sequence = fields.Many2One('ir.sequence',
        'Lessor Contract Sequence', domain=[
            ('company', 'in', [Eval('context', {}).get('company', 0), None]),
            ('sequence_type', '=', Id('real_estate', 'sequence_type_lessor_contract')),
        ], required=True)
    product_admin = fields.Many2One('product.product', 'Product Admin',
            domain=[('salable', '=', True)])
    product_leasing = fields.Many2One('product.product', 'Product Leasing',
            domain=[('salable', '=', True)])
    product_leasing_commercial = fields.Many2One('product.product',
            'Product Leasing Commercial', domain=[('salable', '=', True)])
    product_sale = fields.Many2One('product.product', 'Product Sale',
            domain=[('salable', '=', True)])
    product_commision_leasing = fields.Many2One('product.product',
            'Product Commision Leasing', domain=[('salable', '=', True)])
    product_commision_sale = fields.Many2One('product.product',
            'Product Commision Sale', domain=[('salable', '=', True)])
    product_interest = fields.Many2One('product.product', 'Product Interest',
            domain=[('salable', '=', True)])
    percent_interest = fields.Numeric('Percent Interest')
    product_collection_cost = fields.Many2One('product.product',
            'Product Collection Cost', domain=[('salable', '=', True)])
    percent_collection_cost = fields.Numeric('Percent Collection Cost')
    percent_commision = fields.Numeric('Percent Commision')
    sale_contract_payment_term = fields.Many2One(
            'account.invoice.payment_term', 'Leaseholder Payment Term')
    lessor_contract_payment_term = fields.Many2One('account.invoice.payment_term',
            'Lessor Payment Term')
    sale_contract_type = fields.Many2One('sale.contract.type',
            'Type Sale Contract')
    cuttof_day = fields.Numeric('Cuttof Day', help='Día de corte')
    day_limit_to_pay = fields.Numeric('Day Limit to Pay')
    leaseholder_authorization = fields.Many2One('account.invoice.authorization',
            'Leaseholder Authorization')
    lessor_authorization = fields.Many2One('account.invoice.authorization',
            'Lessor Authorization')
    default_observation_invoice = fields.Text('Default Observation for Invoices')

    @classmethod
    def get_configuration(cls):
        company_id = Transaction().context.get('company')
        if company_id:
            config, = cls.search([
                ('company', '=', company_id),
            ])
            if not config or not config.property_sequence:
                raise AccessError(
                    gettext('real_estate.missing_default_configuration'))
            return config

    # def get_product_account(self):
    #     values = {}
    #     if self.product_admin and self.product_admin.account_category and \
    #         self.product_admin.account_category.account_revenue:
    #             values[self.product_admin.account_category.account_revenue] = 'ADMINISTRACIÓN '
    #     if self.product_leasing and self.product_leasing.account_category and \
    #         self.product_leasing.account_category.account_revenue:
    #             values[self.product_leasing.account_category.account_revenue] = 'CANON DE ARRENDAMIENTO '
    #     if self.product_leasing_commercial and self.product_leasing_commercial.account_category and \
    #         self.product_leasing_commercial.account_category.account_revenue:
    #             values[self.product_leasing_commercial.account_category.account_revenue] = 'CANON DE ARRENDAMIENTO '
    #     return values

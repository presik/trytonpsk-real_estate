# This file is part of the sale_weight module for Tryton.
# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from datetime import date
from decimal import Decimal

from trytond.exceptions import UserError
from trytond.i18n import gettext

# from dateutil.relativedelta import * ??????
from trytond.model import ModelView, fields
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval
from trytond.report import Report
from trytond.transaction import Transaction

MONTHS_STRING = [
    '',
    'ENERO',
    'FEBRERO',
    'MARZO',
    'ABRIL',
    'MAYO',
    'JUNIO',
    'JULIO',
    'AGOSTO',
    'SEPTIEMBRE',
    'OCTUBRE',
    'NOVIEMBRE',
    'DICIEMBRE',
]


class SaleContract(metaclass=PoolMeta):
    __name__ = 'sale.contract'
    property = fields.Many2One('real_estate.property', 'Property',
            required=True)
    destination = fields.Char('Destination')
    months_contract = fields.Function(fields.Numeric('Months Contract'),
            'get_months_contract')

    @classmethod
    def __setup__(cls):
        super(SaleContract, cls).__setup__()
        cls._buttons.update({
            'renovate_contract': {
                'invisible': Eval('state').in_(['canceled', 'finished']),
                },
            })

    def get_rec_name(self, name):
        end_date = str(self.end_date) or ''
        number = self.number or ''
        party = self.party or ''
        rec_name = number + ' [ ' + party.name + ' ]' + ' [ ' + end_date + ' ]'
        return rec_name

    @classmethod
    @ModelView.button
    def renovate_contract(cls, contracts):
        for contract in contracts:
            if contract.end_date:
                year_ = int(contract.end_date.year) + 1
                new_date = date(year_, contract.end_date.month, contract.end_date.day)
                new_value = round(contract.property.cannon_lease + contract.property.get_value_added(), 2)
                contract.update_contracts(new_date, new_value)

    def update_contracts(self, new_date, new_value=0):
        pool = Pool()
        LessorContract = pool.get('real_estate.lessor_contract')
        Configuration = pool.get('real_estate.configuration')
        config = Configuration(1)
        ProductLine = pool.get('sale.contract.product_line')
        if not self.property.lessor_contract:
            # FIXME
            raise UserError(gettext('property_without_lessor_contract'))
        self.write([self], {'end_date': new_date})
        cont, = LessorContract.search([
            ('id', '=', self.property.lessor_contract.id),
        ])
        if cont:
            LessorContract.write([cont], {
                    'cannon_lease': new_value,
                    'end_date': new_date,
                })
            if config and config.product_leasing:
                lines = ProductLine.search([
                    ('product', '=', config.product_leasing.id),
                    ('contract', '=', self.id),
                ])
                for line in lines:
                    ProductLine.write([line], {'unit_price': new_value})

    def get_months_contract(self, name):
        if self.start_date and self.end_date:
            num = round((((self.end_date - self.start_date).days) / 30), 0)
            return int(num)

    def validate_contract(self):
        super(SaleContract, self).validate_contract()
        if not self.property.cannon_lease:
            raise UserError(gettext('property_without_cannon_value'))
        return True

        # pool = Pool()
        # LessorContract = pool.get('real_estate.lessor_contract')
        # Configuration = pool.get('real_estate.configuration')
        # ProductLine = pool.get('sale.contract.product_line')
        # if self.property:
        #     cont, = LessorContract.search([
        #         ('id', '=', self.property.lessor_contract.id),
        #         ('admin_amount', '!=', None),
        #     ])
        #     config = Configuration(1)
        #     if cont and cont.admin_amount:
        #         today_ = date.today()
        #         year_contract = self.start_date + timedelta(days=365)
        #         if today_ > year_contract:
        #             new_admon = cont.admin_amount + self.property.value_added
        #             LessorContract.write([cont], {'admin_amount': new_admon})
        #             if config and config.product_admin:
        #                 lines = ProductLine.search([
        #                     ('product', '=', config.product_admin.id),
        #                 ])
        #                 ProductLine.write(lines, {'unit_price': new_admon})


class SaleContractLine(metaclass=PoolMeta):
    __name__ = 'sale.contract.line'

    def get_sale_line(self, contract, line):
        value = super(SaleContractLine, self).get_sale_line(contract, line)
        month_number = self.date.month
        amount = contract.property.get_amount_days_month(self.date, value['unit_price'])
        description = line.product.name + ' MES ' + MONTHS_STRING[month_number] + ' ' + str(self.date.year)
        value['description'] = description
        value['unit_price'] = round(amount, 2)
        return value

    def validate_contract_line(self):
        if self.sale and self.contract and self.contract.property:
            Invoice = Pool().get('account.invoice')
            Configuration = Pool().get('real_estate.configuration')
            config = Configuration(1)
            # AccountConfiguration = Pool().get('account.configuration')
            # account_config = AccountConfiguration(1)
            if config and config.default_observation_invoice:
                comment = config.default_observation_invoice
            self.sale.comment = comment
            self.sale.save()

            month_ = self.date.month - 1
            year_ = self.date.year
            if self.date.month == 1:
                month_ = 12
                year_ = self.date.year - 1
            date_last_month = date(year_, month_, 1)
            invoices = Invoice.search([
                ('party', '=', self.sale.party.id),
                ('state', 'in', ['posted', 'paid']),
                ('invoice_date', '>=', date_last_month),
            ])
            payment_dates = []
            num_days = 0
            for invoice in invoices:
                if invoice.payment_lines:
                    payment_dates = [payment_line.date for payment_line in invoice.payment_lines if payment_line.date]
                    payment_dates.sort()
                # if invoice.amount_to_pay <= 0:
                #     continue
                if payment_dates and invoice.amount_to_pay == 0:
                    index = len(payment_dates) - 1
                    num_days = int((payment_dates[index] - invoice.invoice_date).days)
                else:
                    num_days = int((self.date - invoice.invoice_date).days)

                _amount_to_pay = 0

                if self.contract.property.day_limit_to_pay and self.contract.property.day_limit_to_pay < num_days:
                    if num_days <= 15:
                        _amount_to_pay = invoice.total_amount * (config.percent_interest / 100) * 15
                    else:
                        _amount_to_pay = invoice.total_amount * (config.percent_interest / 100) * 30

                    if _amount_to_pay > 0:
                        self._create_sale_line(config.product_interest, _amount_to_pay, self.sale, 'INTERESES FACTURA ' + invoice.number)

                        if num_days > 15:
                            _amount_to_pay = self.contract.property.cannon_lease * (config.percent_collection_cost / 100)
                            self._create_sale_line(config.product_collection_cost, _amount_to_pay, self.sale, 'GASTOS DE COBRANZA FACTURA NO.' + invoice.number)

        super(SaleContractLine, self).validate_contract_line()

    def _create_sale_line(self, product, amount, sale, description):
        SaleLine = Pool().get('sale.line')
        new_line = {
            'sale': sale.id,
            'type': 'line',
            'quantity': 1,
            'product': product.id,
            'unit':  product.template.default_uom.id,
            'unit_price': round(Decimal(amount), 2),
            'description':  description,
            'taxes': [],
        }
        taxes_ids = self.get_taxes(sale.party, product)
        if taxes_ids:
            new_line['taxes'] = [('add', taxes_ids)]

        SaleLine.create([new_line])


class RealEstateSaleContractReport(Report):
    __name__ = 'real_estate.sale_contract.report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(RealEstateSaleContractReport, cls).get_context(records, data)
        user = Pool().get('res.user')(Transaction().user)
        report_context['company'] = user.company
        report_context['user'] = user
        date_ = date.today()
        report_context['today'] = date_
        return report_context

# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool, PoolMeta


class Invoice(metaclass=PoolMeta):
    __name__ = 'account.invoice'

    def get_invoices_to_collect(self):
        Invoice = Pool().get('account.invoice')
        invoices = Invoice.search([
            ('party', '=', self.party.id),
            ('state', '=', 'posted'),
        ])
        return [inv for inv in invoices if inv.amount_to_pay != 0]


class InvoiceLine(metaclass=PoolMeta):
    __name__ = 'account.invoice.line'

    def get_property(self):
        if self.origin.sale and self.origin.sale.contract_line:
            return self.origin.sale.contract_line.contract.property
        else:
            return None

    def update_party_revenue(self, product_list=[], product_admin_id=None):
        _property = self.get_property()
        if _property and self.product.id in product_list and _property.lessor_contract:
            self.third_party_income = _property.lessor_contract.proprietary.id
            if product_admin_id and self.product.id == product_admin_id and _property.party_admin:
                self.third_party_income = _property.party_admin.id
            self.save()

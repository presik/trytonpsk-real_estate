# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from datetime import date
from decimal import Decimal

from trytond.model import ModelSQL, ModelView, Workflow, fields
from trytond.pool import Pool
from trytond.pyson import Eval
from trytond.report import Report
from trytond.transaction import Transaction
from trytond.wizard import Button, StateTransition, StateView, Wizard

STATES = {
    'readonly': Eval('state') != 'draft',
}


class LessorContract(Workflow, ModelSQL, ModelView):
    "Property"
    __name__ = 'real_estate.lessor_contract'
    _rec_name = 'number'
    number = fields.Char('Number', readonly=True)
    property = fields.Many2One('real_estate.property', 'Property', states=STATES,
           domain=[('active', '=', True)], required=True)
    proprietary = fields.Many2One('party.party', 'Proprietary', required=True, states=STATES)
    date_contract = fields.Date('Date Contract', required=True, states=STATES)
    start_date = fields.Date('Start Contract', required=True, states=STATES)
    end_date = fields.Date('End Contract', states=STATES)
    # location = fields.Char('City', states=STATES)
    cannon_lease = fields.Numeric('Cannon Lease', states={
        'required': Eval('kind') == 'leasing',
        'readonly': Eval('state') != 'draft',
    })
    sale_price_list = fields.Numeric('Sale Price', states={
        'required': Eval('kind') == 'sale',
        'readonly': Eval('state') != 'draft',
    })
    admin_amount = fields.Numeric('Administration Amount', states=STATES)
    percent_commision = fields.Numeric('Percent Commision', digits=(4, 2), states=STATES)
    legal_information = fields.Text('Legal Information', states=STATES)
    state = fields.Selection([
            ('draft', 'Draft'),
            ('active', 'Active'),
            ('finished', 'Finished'),
        ], 'State', readonly=True)
    kind = fields.Selection([
            ('', ''),
            ('leasing', 'Leasing'),
            ('sale', 'Sale'),
        ], 'State', required=True, states=STATES)
    country = fields.Many2One('country.country', 'Country', states=STATES)
    subdivision = fields.Many2One('country.subdivision',
            'Subdivision', domain=[
                ('country', '=', Eval('country')),
            ], states=STATES)
    city = fields.Many2One('country.city', 'City',
            domain=[
                ('subdivision', '=', Eval('subdivision')),
            ], states=STATES)
    observation = fields.Text('Observation', states=STATES)
    contract_date_words = fields.Function(fields.Char('Contract Date Words'),
        'get_contract_date_words')
    sales = fields.Function(fields.One2Many('sale.sale', None, 'Sales',
        readonly=True), 'get_sales')
    day_to_pay = fields.Numeric('Day to Pay', digits=(2, 0))
    description = fields.Char('Description', required=True)

    @classmethod
    def __setup__(cls):
        super(LessorContract, cls).__setup__()
        cls._order = [
            ('date_contract', 'ASC'),
            ('state', 'ASC'),
            ('number', 'ASC'),
        ]
        # cls._order.insert(0, ('product.name', 'ASC'))
        # cls._order.insert(1, ('product.code', 'ASC'))
        cls._transitions |= set((
                ('draft', 'active'),
                ('active', 'draft'),
                ('active', 'finished'),
                ('finished', 'active'),
                ))
        cls._buttons.update({
            'draft': {
                'invisible': Eval('state') != 'active',
                },
            'active': {
                'invisible': Eval('state') == 'active',
                },
            'finished': {
                'invisible': Eval('state') != 'active',
                },
        })

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('active')
    def active(cls, records):
        cls.set_number(records)
        Contract = Pool().get('sale.contract')
        Property = Pool().get('real_estate.property')
        for record in records:
            if record.property and record.property.state == 'finished':
                Property.write([record.property], {'state': None})
            if record.property.current_leaseholder_contract and record.property.current_leaseholder_contract.state == 'finished':
                Contract.write([record.property.current_leaseholder_contract], {'state': 'confirmed'})
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('finished')
    def finished(cls, records):
        Contract = Pool().get('sale.contract')
        for record in records:
            if record.property.current_leaseholder_contract:
                Contract.write(
                    [record.property.current_leaseholder_contract],
                    {'state': 'finished'},
                )

    def get_rec_name(self, name):
        end_date = str(self.end_date) or ''
        number = self.number or ''
        proprietary = self.proprietary or ''
        return number + ' [ ' + proprietary.name + ' ]' + ' [ ' + end_date + ' ]'

    def create_sale(self, contract, start_date, end_date, payment_term_id=None):
        pool = Pool()
        Sale = pool.get('sale.sale')
        SaleLine = pool.get('sale.line')
        Party = pool.get('party.party')
        Configuration = pool.get('real_estate.configuration')
        LessorContractSale = pool.get('real_estate.lessor_contract-sale')
        config = Configuration.get_configuration()
        sales = Sale.search([
            ('lessor_contract', '=', contract.id),
            ('sale_date', '>=', start_date),
            ('sale_date', '<=', end_date),
        ])
        if sales:
            return
        date_ = start_date
        if contract.kind == 'leasing' and contract.property.current_leaseholder_contract:
            start_date_sale_contract = contract.property.current_leaseholder_contract.start_date
        else:
            start_date_sale_contract = contract.start_date
        if start_date_sale_contract > date_:
            date_ = start_date_sale_contract

        unit_price = Decimal('0.0')
        product_commision = None

        if self.state == 'active' and self.kind:
            if self.kind == 'leasing':
                product_commision = config.product_commision_leasing
                unit_price = (self.cannon_lease * (self.percent_commision / 100))
            elif self.kind == 'sale' and config.product_commision_sale:
                product_commision = config.product_commision_sale
                unit_price = (self.sale_price_list * (self.percent_commision / 100))

            if self.proprietary.customer_payment_term:
                payment_term_id = self.proprietary.customer_payment_term.id

            _description = self.property.address if self.property.address else ''
            if self.description:
                _description = self.description + ' ' + _description

            sale = Sale(
                company=self.property.company.id,
                party=self.proprietary.id,
                price_list=None,
                sale_date=date_,
                state='draft',
                description=_description,
                payment_term=payment_term_id,
                invoice_address=Party.address_get(self.proprietary, type='invoice'),
                shipment_address=Party.address_get(self.proprietary, type='delivery'),
                reference='No.' + self.number,
                lessor_contract=self,
                invoice_type='1',
            )
            months_string = ['', 'ENERO', 'FEBRERO', 'MARZO', 'ABRIL', 'MAYO', 'JUNIO', 'JULIO', 'AGOSTO', 'SEPTIEMBRE', 'OCTUBRE', 'NOVIEMBRE', 'DICIEMBRE']
            month_number = date_.month
            desc_add = ' MES ' + months_string[month_number] + ' ' + str(date_.year)

            sale.save()
            if self.kind == 'leasing':
                unit_price = self.property.get_amount_days_month(date_, unit_price)
            new_line = {
                'sale': sale.id,
                'type': 'line',
                'quantity': 1,
                'product': product_commision.id,
                'unit': product_commision.template.default_uom.id,
                'unit_price': round(Decimal(unit_price), 2),
                'description': product_commision.description + desc_add or product_commision.name + desc_add or '',
                'taxes': [],
            }
            taxes_ids = self.get_taxes(self.proprietary, product_commision)
            if taxes_ids:
                new_line['taxes'] = [('add', taxes_ids)]
            SaleLine.create([new_line])
            Sale.quote([sale])
            Sale.confirm([sale])
            Sale.process([sale])
            new_sale = {
                'sale': sale.id,
                'lessor_contract': self,
            }
            LessorContractSale.create([new_sale])
            # self.post_invoices(sale)

    # def post_invoices(self, sale):
    #     for inv in sale.invoices:
    #         inv.post([inv])

    def get_taxes(self, party, product):
        taxes = []
        pattern = {}
        for tax in product.customer_taxes_used:
            if party.customer_tax_rule:
                tax_ids = party.customer_tax_rule.apply(tax, pattern)
                if tax_ids:
                    taxes.extend(tax_ids)
                continue
            taxes.append(tax.id)
        if party.customer_tax_rule:
            for line in party.customer_tax_rule.lines:
                tax_ids = party.customer_tax_rule.apply(line.tax, pattern)
                if tax_ids:
                    taxes.extend(tax_ids)

        return taxes

    @classmethod
    def set_number(cls, requests):
        """
        Fill the number field with the lessor contract sequence
        """
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        Config = pool.get('real_estate.configuration')
        config = Config.get_configuration()

        for request in requests:
            if request.number:
                continue
            if not config.lessor_contract_sequence:
                continue
            number = Sequence.get(config.lessor_contract_sequence)
            cls.write([request], {'number': number})

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_contract_date():
        Date = Pool().get('ir.date')
        return Date.today

    @staticmethod
    def default_legal_information():
        Configuration = Pool().get('real_estate.configuration')
        config = Configuration(1)
        if config and config.legal_information:
            return config.legal_information
        else:
            return ''

    @staticmethod
    def default_percent_commision():
        Configuration = Pool().get('real_estate.configuration')
        config = Configuration(1)
        if config and config.percent_commision:
            return config.percent_commision
        else:
            return 0

    @staticmethod
    def default_country():
        CountryCode = Pool().get('country.country')
        countries = CountryCode.search([
            ('code', '=', '169'),
        ])
        if countries:
            return countries[0].id

    @fields.depends('date_contract', 'start_date')
    def on_change_date_contract(self):
        if self.date_contract:
            self.start_date = self.date_contract

    # def get_percent_admin(self, name):
    #     if self.admin_amount and self.cannon_lease:
    #         return round((self.admin_amount / self.cannon_lease), 2)

    @fields.depends('property', 'subdivision', 'city')
    def on_change_property(self):
        if self.property:
            if self.property.subdivision:
                self.subdivision = self.property.subdivision.id
            if self.property.city:
                self.city = self.property.city.id
            if self.property.proprietary:
                proprietary_ids = [p.id for p in self.property.proprietary]
                self.proprietary = proprietary_ids[0]
        else:
            self.subdivision = None
            self.city = None
            self.proprietary = None

    def get_contract_date_words(self, name):
        if self.date_contract and self.property:
            return self.property.get_message_date(self.date_contract)
        else:
            return ''

    def get_sales(self, name):
        Sale = Pool().get('sale.sale')
        sales = Sale.search([
            ('lessor_contract', '=', self.id),
        ])
        if sales:
            return [s.id for s in sales]

    # @fields.depends('state', 'number')
    # def on_change_state(self):
    #     pool = Pool()
    #     Sequence = pool.get('ir.sequence')
    #     Config = pool.get('real_estate.configuration')
    #     config = Config(1)
    #     if self.state == 'active':
    #         for request in requests:
    #             if request.number:
    #                 continue
    #             if not config.lessor_contract_sequence:
    #                 continue
    #             number = Sequence.get_id(config.lessor_contract_sequence.id)
    #             # cls.write([request], {'number': number})
    #             self.number = number


class LessorContractSale(ModelSQL):
    "LessorContract - Sale"
    __name__ = 'real_estate.lessor_contract-sale'
    _table = 'lessor_contract_sales_rel'
    lessor_contract = fields.Many2One('real_estate.lessor_contract',
        'Contract', ondelete='CASCADE', required=True)
    sale = fields.Many2One('sale.sale', 'Sale', ondelete='RESTRICT',
        required=True)


class LessorContractReport(Report):
    __name__ = 'real_estate.lessor_contract.report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super().get_context(records, data)
        user = Pool().get('res.user')(Transaction().user)
        report_context['company'] = user.company
        report_context['user'] = user
        date_ = date.today()
        report_context['today'] = date_
        return report_context


class CreateInvoicesProprietaryStart(ModelView):
    "Create Invoices Proprietary Start"
    __name__ = 'real_estate.create_invoices_proprietary.start'
    fiscalyear = fields.Many2One('account.fiscalyear', 'Fiscal Year',
            required=True)
    company = fields.Many2One('company.company', 'Company', required=True)
    period = fields.Many2One('account.period', 'Period',
            depends=['fiscalyear'], required=True, domain=[
                ('fiscalyear', '=', Eval('fiscalyear')),
            ])
    # payment_term = fields.Many2One('account.invoice.payment_term',
    #         'Payment Term', required=True)

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @fields.depends('fiscalyear')
    def on_change_fiscalyear(self):
        self.period = None

    @staticmethod
    def default_fiscalyear():
        FiscalYear = Pool().get('account.fiscalyear')
        return FiscalYear.find(
            Transaction().context.get('company'), exception=False)


class CreateInvoicesProprietary(Wizard):
    "Create Invoices Proprietary"
    __name__ = 'real_estate.create_invoices_proprietary'
    start = StateView(
        'real_estate.create_invoices_proprietary.start',
        'real_estate.create_invoices_proprietary_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Create', 'accept', 'tryton-ok', default=True),
        ])
    accept = StateTransition()

    def transition_accept(self):
        pool = Pool()
        LessorContract = pool.get('real_estate.lessor_contract')
        Configuration = pool.get('real_estate.configuration')
        config = Configuration.get_configuration()
        contracts = LessorContract.search([
            ('state', '=', 'active'),
            ('property.active', '=', 'true'),
            ['AND', ['OR', [
                    ('end_date', '>=', self.start.period.start_date),
                ],
                [
                    ('end_date', '=', None),
                ],
            ]],
        ])

        for contract in contracts:
            contract.create_sale(
                contract,
                self.start.period.start_date,
                self.start.period.end_date,
                config.lessor_contract_payment_term.id)

        return 'end'


class CreateInvoiceLessor(Wizard):
    "Create Invoice"
    __name__ = 'real_estate.lessor_contract.create_invoice'
    start_state = 'create_invoice'
    create_invoice = StateTransition()

    def transition_create_invoice(self):
        pool = Pool()
        LessorContract = pool.get('real_estate.lessor_contract')
        config = pool.get('real_estate.configuration').get_configuration()
        Date = pool.get('ir.date')
        start = Date.today()
        end = date(start.year, (start.month + 1), start.day)
        id_ = Transaction().context['active_id']
        for contract in LessorContract.browse([id_]):
            contract.create_sale(
                contract,
                start,
                end,
                config.lessor_contract_payment_term.id)
        return 'end'

# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from datetime import date, timedelta

from trytond.exceptions import UserError
from trytond.i18n import gettext
from trytond.model import ModelSQL, ModelView, fields
from trytond.pool import Pool
from trytond.pyson import Bool, Eval, If
from trytond.report import Report
from trytond.transaction import Transaction
from trytond.wizard import Button, StateReport, StateTransition, StateView, Wizard

conversor = None
try:
    from numword import numword_es
    conversor = numword_es.NumWordES()
except:
    print("Warning: Does not possible import numword module, please install it...!")


STATES = {
    'readonly': Eval('state') != 'draft',
}


class PropertyCheckList(ModelSQL, ModelView):
    "Property Check List"
    __name__ = 'real_estate.check_list'
    property = fields.Many2One('real_estate.property', 'Property',
        required=True)
    item = fields.Many2One('real_estate.item_check_list', 'Item',
        required=True)
    value = fields.Char('Value', required=True)


class KindProperty(ModelSQL, ModelView):
    "Kind Property"
    __name__ = 'real_estate.kind_property'
    name = fields.Char('Name')
    ipc_plus_required = fields.Boolean('IPC Plus Required')


class ItemCheckList(ModelSQL, ModelView):
    "Item Check List"
    __name__ = 'real_estate.item_check_list'
    name = fields.Char('Name')


class Property(ModelSQL, ModelView):
    "Property"
    __name__ = 'real_estate.property'
    _rec_name = 'code'
    code = fields.Char('Code', readonly=True)
    active = fields.Boolean('Active')
    kind = fields.Many2One('real_estate.kind_property', 'Kind', required=True)
    proprietary = fields.Many2Many('real_estate.property-party', 'property',
        'party', 'Proprietaries', required=True)
    debtors = fields.Many2Many('real_estate.debtor-party', 'property', 'party',
        'debtors')
    company = fields.Many2One('company.company', 'Company', required=True,
        domain=[
            ('id', If(Eval('context', {}).contains('company'), '=', '!='),
                Eval('context', {}).get('company', 0)),
            ])
    # contact_mechanisms = fields.Function(fields.One2Many('party.contact_mechanism', None,
    #         'Contact Mechanisms', states={'invisible': ~Bool(Eval('proprietary'))},
    #             depends=['proprietary']), 'get_contact_mechanisms')
    neighborhood = fields.Char('Neighborhood')
    address = fields.Char('Address')
    country = fields.Many2One('country.country', 'Country')
    subdivision = fields.Many2One('country.subdivision', 'Subdivision', domain=[
                ('country', '=', Eval('country')),
            ])
    city = fields.Many2One('country.city', 'City',
            domain=[
                ('subdivision', '=', Eval('subdivision')),
            ])
    postal_code = fields.Char('Postal Code')
    note = fields.Text('Notes')
    registration_number = fields.Char('Registration Number')
    stratum = fields.Char('Stratum')
    valuation_property = fields.Numeric('Valuation Property')
    # Contrato arrendador
    lessor_contract = fields.Function(fields.Many2One(
            'real_estate.lessor_contract', 'Lessor Contract'),
            'get_lessor_contract')
    state = fields.Selection([
            ('', ''),
            ('available', 'Available'),
            ('leased', 'Leased'),
            ('sold', 'Sold'),
            ('finished', 'Finished'),
        ], 'State')
    contracts = fields.Function(
            fields.One2Many('sale.contract', None, 'Contracts'),
            'get_sale_contracts')
    current_leaseholder_contract = fields.Function(
        fields.Many2One('sale.contract', 'Current Contract'),
        'get_current_leaseholder_contract')
    check_list = fields.One2Many('real_estate.check_list', 'property',
            'Check_list')
    area = fields.Numeric('Area')
    sale_price_list = fields.Function(fields.Numeric('Sale Price',
        help="This field depend of lessor contract",
        depends=['lessor_contract'],
        states={
            'invisible': ~Bool(Eval('lessor_contract')),
        }), 'get_sale_price')
    cannon_lease = fields.Function(fields.Numeric('Cannon Lease',
        help="This field depend of lessor contract",
        depends=['lessor_contract'],
        states={
            'invisible': ~Bool(Eval('lessor_contract')),
        }), 'get_cannon_lease')
    admin_amount = fields.Function(fields.Numeric('Admin Amount',
        help="This field depend of lessor contract",
        depends=['lessor_contract'], states={
          'invisible': ~Bool(Eval('lessor_contract')),
        }), 'get_admin_amount')
    admin_amount_words = fields.Function(fields.Char('Admin Amount Words'),
            'get_amount_words')
    cannon_lease_words = fields.Function(fields.Char('Cannon Lease Words'),
            'get_amount_words')
    kind_lessor_contract = fields.Function(fields.Char('Kind Lessor Contract',
        readonly=True), 'get_kind_lessor_contract')
    percent_commision = fields.Function(fields.Numeric('Percent Commision',
        help="This field depend of lessor contract",
        depends=['lessor_contract'], states={
           'invisible': ~Bool(Eval('lessor_contract')),
        }), 'get_percent_commision')
    cuttof_day = fields.Numeric('Cuttof Day', digits=(2, 0))
    day_limit_to_pay = fields.Numeric('Day Limit to Pay', digits=(2, 0))
    # value_added = fields.Function(fields.Numeric('Value Added '), 'get_value_added')
    party_admin = fields.Many2One('party.party',
        'Party Administration of Property',
        help="This field depend of lessor contract",
        depends=['lessor_contract'], states={
            'invisible': ~Bool(Eval('lessor_contract')),
        })

    @classmethod
    def __setup__(cls):
        super(Property, cls).__setup__()
        # cls._error_messages.update({
        #         'without_leaseholder_contract': ('The property must have a leaseholder Contract'),
        #         })
        cls._buttons.update({
            'wizard_create_leaseholder_contract': {},
            'wizard_create_lessor_contract': {},
        })

    @classmethod
    @ModelView.button_action('real_estate.wizard_create_leaseholder_contract')
    def wizard_create_leaseholder_contract(cls, records):
        pass

    @classmethod
    @ModelView.button_action('real_estate.wizard_create_lessor_contract')
    def wizard_create_lessor_contract(cls, records):
        pass

    @staticmethod
    def default_state():
        return 'available'

    @staticmethod
    def default_cuttof_day():
        Config = Pool().get('real_estate.configuration')
        config = Config.get_configuration()
        if config and config.cuttof_day:
            return config.cuttof_day

    @staticmethod
    def default_day_limit_to_pay():
        Config = Pool().get('real_estate.configuration')
        config = Config.get_configuration()
        if config and config.day_limit_to_pay:
            return config.day_limit_to_pay

    @staticmethod
    def default_country():
        Country = Pool().get('country.country')
        country, = Country.search([
            ('code', '=', 'CO'),
        ])
        if country:
            return country.id

    def get_rec_name(self, name):
        city = self.city.name if self.city else ''
        address = self.address or ''
        kind = self.kind.name if self.kind else ''
        # self._rec_name = code
        _rec_name = kind + '[' + address + ']' + '[' + city + ']'
        return _rec_name

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        return [
            bool_op,
            ('code',) + tuple(clause[1:]),
            ('neighborhood',) + tuple(clause[1:]),
            ('address',) + tuple(clause[1:]),
        ]

    # def pre_validate(self):
    #     if not self.current_leaseholder_contract and \
    #         self.state not in ['available', '']:
    #         self.raise_user_error('without_leaseholder_contract')

    @classmethod
    def validate(cls, records):
        for record in records:
            if not record.code:
                cls.set_number(record)

    @classmethod
    def set_number(cls, request):
        """
        Fill the number field with the property sequence
        """
        Config = Pool().get('real_estate.configuration')
        config = Config.get_configuration()
        if request.code:
            return
            if not config.property_sequence:
                return

        pool = Pool()
        Sequence = pool.get('ir.sequence')
        number = Sequence.get(config.property_sequence)
        cls.write([request], {'code': number})

    @staticmethod
    def default_company():
        return Transaction().context.get('company') or None

    @staticmethod
    def default_active():
        return True

    def get_amount_words(self, name):
        if conversor:
            if name == 'admin_amount_words' and self.admin_amount:
                num = (conversor.cardinal(int(self.admin_amount))).upper()
            if name == 'cannon_lease_words' and self.cannon_lease:
                num = (conversor.cardinal(int(self.cannon_lease))).upper()
            return num

    def get_contact_mechanisms(self, name):
        if self.proprietary:
            proprietaries = [p for p in self.proprietary]
            return [c.id for pr in proprietaries for c in pr.contact_mechanisms]
        return []

    def get_lessor_contract(self, name):
        LessorContract = Pool().get('real_estate.lessor_contract')
        contracts = LessorContract.search([
            ('property', '=', self),
            ('state', '=', 'active'),
        ])
        if contracts:
            return contracts[0].id

    def get_sale_contracts(self, name):
        Contract = Pool().get('sale.contract')
        contracts = Contract.search([
            ('property', '=', self),
        ])
        return [c.id for c in contracts]

    def get_current_leaseholder_contract(self, name):
        Contract = Pool().get('sale.contract')
        contracts = Contract.search([
            ('property', '=', self.id),
            ('state', '=', 'confirmed'),
        ])
        if contracts:
            ind = len(contracts)
            return contracts[ind - 1].id

    def get_cannon_lease(self, name):
        if self.lessor_contract:
            return self.lessor_contract.cannon_lease

    def get_value_added(self):
        Configuration = Pool().get('real_estate.configuration')
        config = Configuration(1)
        if self.kind and config and self.cannon_lease:
            if self.kind.ipc_plus_required:
                return (self.cannon_lease * (config.ipc_value + config.ipc_plus)) / 100
            else:
                return (self.cannon_lease * config.ipc_value) / 100
        return 0

    def get_admin_amount(self, name):
        if self.lessor_contract:
            return self.lessor_contract.admin_amount

    def get_kind_lessor_contract(self, name):
        if self.lessor_contract and self.lessor_contract.kind:
            return self.lessor_contract.kind

    def get_percent_commision(self, name):
        if self.lessor_contract:
            return self.lessor_contract.percent_commision

    def get_sale_price(self, name):
        if self.lessor_contract:
            return self.lessor_contract.sale_price_list

    def get_message_date(self, date_):
        months = ("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre")
        day_ = date_.day
        month_ = date_.month
        year_ = date_.year
        day_string, year_string = str(day_), str(year_)
        month_string = months[month_ - 1]
        if conversor:
            day_string = (conversor.cardinal(int(day_)))
            year_string = (conversor.cardinal(int(year_)))

        return f"{day_string}({day_}) días de {month_string} del año {year_string}({year_})"

    def get_amount_days_month(self, date_, amount):
        days_month = 0
        month_number = date_.month
        if self.current_leaseholder_contract and self.current_leaseholder_contract.end_date:
            days_month = (self.current_leaseholder_contract.end_date - date_).days + 1
        if date_.day != 1:
            days_month = 30 - int(date_.day) + 1
        if days_month:
            if month_number == 2 and days_month < 28:
                amount = (amount / 30) * days_month
            else:
                if days_month < 30:
                    amount = (amount / 30) * days_month
        return amount


class ProprietaryParty(ModelSQL):
    "Proprietary Party"
    __name__ = 'real_estate.property-party'
    _table = 'real_estate_property_party_rel'
    property = fields.Many2One('real_estate.property', 'Property',
        ondelete='CASCADE', required=True)
    party = fields.Many2One('party.party', 'Party', ondelete='RESTRICT',
        required=True)


class DebtorsParty(ModelSQL):
    "Debtors Party"
    __name__ = 'real_estate.debtor-party'
    _table = 'real_estate_debtor_party_rel'
    property = fields.Many2One('real_estate.property', 'Property',
        ondelete='CASCADE', required=True)
    party = fields.Many2One('party.party', 'Party', ondelete='RESTRICT',
        required=True)


class CreateLeaseholderContractStart(ModelView):
    "Create Leaseholder Contract Start"
    __name__ = 'real_estate.create_leaseholder_contract.start'
    party = fields.Many2One('party.party', 'Party', required=True)
    start_date = fields.Date('Start Date', required=True)
    end_date = fields.Date('End Date')
    payment_term = fields.Many2One('account.invoice.payment_term',
        'Payment Term', required=True)
    salesman = fields.Many2One('company.employee', 'Salesman')
    destination = fields.Char('Destination')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_payment_term():
        Configuration = Pool().get('real_estate.configuration')
        config = Configuration(1)
        if config and config.sale_contract_payment_term:
            return config.sale_contract_payment_term.id

    @fields.depends('start_date', 'end_date')
    def on_change_start_date(self):
        if self.start_date:
            self.end_date = self.start_date + timedelta(days=365)


class CreateLeaseholderContract(Wizard):
    "Create Leaseholder Contract"
    __name__ = 'real_estate.create_leaseholder_contract'
    start = StateView(
        'real_estate.create_leaseholder_contract.start',
        'real_estate.create_leaseholder_contract_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Create', 'accept', 'tryton-ok', default=True),
        ])
    accept = StateTransition()

    @classmethod
    def __setup__(cls):
        super(CreateLeaseholderContract, cls).__setup__()
        # cls._error_messages.update({
        #         'without_lessor_contract': ('The property not have Lessor Contract'),
        #         'exist_leaseholder_contract': ('Already exists leaseholder contract active'),
        #         })

    def transition_accept(self):
        pool = Pool()
        Contract = pool.get('sale.contract')
        Configuration = pool.get('real_estate.configuration')
        Property = pool.get('real_estate.property')
        Date = pool.get('ir.date')
        ids = Transaction().context['active_ids']
        config = Configuration(1)

        properties = Property.search([('id', 'in', ids)])
        today = Date.today()
        for p in properties:
            if not p.lessor_contract:
                raise UserError(gettext('without_lessor_contract'))
            for contract in p.contracts:
                if contract.state == 'confirmed':
                    if not contract.end_date or contract.end_date >= today:
                        raise UserError(gettext('exist_leaseholder_contract'))
                    Contract.finish([contract])
                if contract.state == 'draft':
                    Contract.cancel([contract])

            contract_to_create = {
                'company': p.company.id,
                'party': self.start.party.id,
                'contract_date': today,
                'payment_term': self.start.payment_term.id or config.sale_contract_payment_term.id or None,
                'type': config.sale_contract_type.id or None,
                'start_date': self.start.start_date,
                'end_date':  self.start.end_date or None,
                'property':  p.id,
                'comment':  p.note or '',
                'salesman':  self.start.salesman.id if self.start.salesman else None,
                'total_amount':  1,
                'description': 'CONTRATO INMOBILIARIO DE LA PROPIEDAD ' + p.neighborhood + ' ' + p.address,
                'invoices_number': 1,
                'invoice_method': 'order',
                'state': 'draft',
                'product_lines': [],
                'monthly_cutoff_date': p.cuttof_day or None,
                'destination': self.start.destination or None,
            }
            lines_to_create = []
            description, product_id, unit_price = '', None, 0
            if p.kind_lessor_contract == 'leasing':
                if p.kind.ipc_plus_required:
                    description = config.product_leasing_commercial.name or None
                    product_id = config.product_leasing_commercial.id
                else:
                    description = config.product_leasing.name or None
                    product_id = config.product_leasing.id
                product_commision = config.product_commision_leasing
                unit_price = p.cannon_lease
            elif p.kind_lessor_contract == 'sale':
                description = config.product_sale.name or None
                product_id = config.product_sale.id
                product_commision = config.product_commision_sale
                unit_price = p.sale_price_list

            value = {
                # 'analytic_account': None,
                'description': description,
                'product': product_id,
                'type': 'line',
                'unit_price': unit_price,
                'only_first_invoice': False,
            }
            lines_to_create.append(value)
            if p.admin_amount and p.admin_amount > 0 and config.product_admin:
                lines_to_create.append({
                    # 'analytic_account': None,
                    'description': config.product_admin.description or config.product_admin.name,
                    'product': config.product_admin.id,
                    'type': 'line',
                    'unit_price': p.admin_amount,
                    'only_first_invoice': False,
                })
            contract_to_create['product_lines'] = [('create', lines_to_create)]
            contract, = Contract.create([contract_to_create])
            Contract.confirm([contract])
        return 'end'


class CreateLessorContractStart(ModelView):
    "Create Lessor Contract Start"
    __name__ = 'real_estate.create_lessor_contract.start'
    start_date = fields.Date('Start Date', required=True)
    end_date = fields.Date('End Date')
    description = fields.Char('Description', required=True)
    day_to_pay = fields.Numeric('Day to Pay', digits=(2, 0))
    kind = fields.Selection([
            ('', ''),
            ('leasing', 'Leasing'),
            ('sale', 'Sale'),
        ], 'Kind Contract', required=True)
    cannon_lease = fields.Numeric('Cannon Lease')
    sale_price_list = fields.Numeric('Sale Price')
    admin_amount = fields.Numeric('Administration Amount')

    @staticmethod
    def default_kind():
        return 'leasing'

    @staticmethod
    def default_day_to_pay():
        return 1

    @staticmethod
    def default_start_date():
        Date = Pool().get('ir.date')
        return Date.today()

    @fields.depends('start_date', 'end_date')
    def on_change_start_date(self):
        if self.start_date:
            self.end_date = self.start_date + timedelta(days=365)


class CreateLessorContract(Wizard):
    "Create Lessor Contract"
    __name__ = 'real_estate.create_lessor_contract'
    start = StateView(
        'real_estate.create_lessor_contract.start',
        'real_estate.create_lessor_contract_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Create', 'accept', 'tryton-ok', default=True),
        ])
    accept = StateTransition()

    @classmethod
    def __setup__(cls):
        super(CreateLessorContract, cls).__setup__()
        # cls._error_messages.update({
        #         'without_proprietary': ('The property without Proprietary'),
        #         })

    def transition_accept(self):
        pool = Pool()
        Contract = pool.get('real_estate.lessor_contract')
        Property = pool.get('real_estate.property')
        id_ = Transaction().context['active_id']

        if not id_:
            return 'end'

        properties = Property.search([('id', '=', id_)])
        for p in properties:
            if not p.proprietary:
                raise UserError(gettext('without_proprietary'))
            if p.lessor_contract:
                Contract.finished([p.lessor_contract])

            create_contract = {
                'property': p.id,
                'proprietary': p.proprietary[0].id,
                'date_contract': self.start.start_date,
                'start_date': self.start.start_date,
                'end_date': self.start.end_date,
                'cannon_lease': self.start.cannon_lease,
                'sale_price_list': self.start.sale_price_list,
                'admin_amount': self.start.admin_amount,
                'kind': self.start.kind,
                'day_to_pay': self.start.day_to_pay,
                'description': self.start.description,
            }
            contract, = Contract.create([create_contract])
            contract.on_change_property()
            contract.save()
            Contract.active([contract])
        return 'end'


class FinishContractReport(Report):
    __name__ = 'real_estate.finish_contract.report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super().get_context(records, data)
        user = Pool().get('res.user')(Transaction().user)
        report_context['company'] = user.company
        report_context['user'] = user
        date_ = date.today()
        report_context['today'] = date_
        return report_context


class PropertyReportStart(ModelView):
    "Property Report Start"
    __name__ = 'real_estate.property_report.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    subdivision = fields.Many2One('country.subdivision', 'Subdivision')
    city = fields.Many2One('country.city', 'City', domain=[
                ('subdivision', '=', Eval('subdivision')),
            ])

    @staticmethod
    def default_subdivision():
        Subdivision = Pool().get('country.subdivision')
        subdivision, = Subdivision.search([
            ('code', '=', 'CO'),
        ])
        if subdivision:
            return subdivision.id

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_end_date():
        Date = Pool().get('ir.date')
        return Date.today()


class PropertyReportWizard(Wizard):
    "Property Report Wizard"
    __name__ = 'real_estate.property_report_wizard'
    start = StateView(
        'real_estate.property_report.start',
        'real_estate.property_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok', default=True),
        ])
    print_ = StateReport('real_estate.property_report')

    def do_print_(self, action):
        city_id, subdivision_id = None, None
        if self.start.subdivision:
            subdivision_id = self.start.subdivision.id
        if self.start.city:
            city_id = self.start.city.id
        data = {
            'company': self.start.company.id,
            'subdivision': subdivision_id,
            'city': city_id,
        }
        return action, data

    def transition_print_(self):
        return 'end'


class PropertyReport(Report):
    __name__ = 'real_estate.property_report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super().get_context(records, data)
        pool = Pool()
        Date = pool.get('ir.date')
        today_ = Date.today()
        user = Pool().get('res.user')(Transaction().user)
        Company = pool.get('company.company')
        Property = pool.get('real_estate.property')
        LessorContract = pool.get('real_estate.lessor_contract')

        lessor_contracts = LessorContract.search([
            ('kind', '!=', 'leasing'),
            ('state', '!=', 'active'),
        ])
        property_ids = [lc.property.id for lc in lessor_contracts if lc.property]

        property_filtered = [
            ('company', '=', data['company']),
            ('id', 'not in', property_ids),
        ]

        if data['subdivision']:
            property_filtered.append(
                ('subdivision', '=', data['subdivision']),
            )
        if data['city']:
            property_filtered.append(
                ('city', '=', data['city']),
            )

        properties = Property.search(
            property_filtered,
            order=[('city.name', 'ASC')],
        )
        for p in properties:
            if p.proprietary:
                proprietary_names = [pr.name for pr in p.proprietary]
                proprietary_phones = [pr.phone for pr in p.proprietary]
                proprietary_emails = [pr.email for pr in p.proprietary]
                _names = ";".join(proprietary_names) or ""
                _phones = ";".join(proprietary_phones) or ""
                _emails = ";".join(proprietary_emails) or ""
                p.proprietary_names = _names
                p.proprietary_phones = _phones
                p.proprietary_emails = _emails
            # if p.debtors:
            #     debtors_names = [pr.name for pr in p.debtors]
            #     debtors_phones = [pr.phone for pr in p.debtors]
            #     debtors_emails = [pr.email for pr in p.debtors]
            #     _names = ";".join(debtors_names) or ""
            #     _phones = ";".join(debtors_phones) or ""
            #     _emails = ";".join(debtors_emails) or ""
            #     setattr(p, 'debtors_names', _names)
            #     setattr(p, 'debtors_phones', _phones)
            #     setattr(p, 'debtors_emails', _emails)
        report_context['total_cannon'] = sum(
                p.cannon_lease for p in properties if p.cannon_lease)
        report_context['total_admin'] = sum(
                p.admin_amount for p in properties if p.admin_amount)
        report_context['total_commision'] = sum(
            (p.percent_commision * p.cannon_lease) / 100 for p in properties if p.percent_commision and p.cannon_lease)
        report_context['records'] = properties
        report_context['company'] = Company(data['company'])
        report_context['user'] = user
        report_context['today'] = today_
        return report_context


class RealEstateDunningReport(Report):
    __name__ = 'real_estate.party_dunning'

    @classmethod
    def get_context(cls, records, data):
        report_context = super().get_context(records, data)
        pool = Pool()
        Company = pool.get('company.company')
        Invoice = pool.get('account.invoice')

        _records = []
        now = date.today()
        for property in records:
            if not property.current_leaseholder_contract:
                continue
            invoices = Invoice.search([
                ('party', '=', property.current_leaseholder_contract.party.id),
                ('state', 'in', ['posted', 'validated']),
                ('type', '=', 'out'),
                ], order=[('invoice_date', 'ASC'), ('number', 'ASC')])

            posted_invoices = []
            sum_invoices = []
            sum_amount_to_pay = []

            for invoice in invoices:
                sum_invoices.append(invoice.total_amount)
                sum_amount_to_pay.append(invoice.amount_to_pay)
                aged = ''
                if invoice.invoice_date:
                    aged = (now - invoice.invoice_date).days
                invoice.aged = aged
                posted_invoices.append(invoice)

            property.invoices = posted_invoices
            property.sum_invoices = sum(sum_invoices)
            property.sum_amount_to_pay = sum(sum_amount_to_pay)
            _records.append(property)

        report_context['records'] = _records
        report_context['company'] = Company(1)
        return report_context

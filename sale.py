# This file is part of the sale_weight module for Tryton.
# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval

STATES = {
    'readonly': Eval('state') != 'draft',
}


class Sale(metaclass=PoolMeta):
    __name__ = 'sale.sale'
    lessor_contract = fields.Many2One('real_estate.lessor_contract',
        'Lessor Contract', states=STATES)

    @classmethod
    def process(cls, sales):
        super(Sale, cls).process(sales)
        Configuration = Pool().get('real_estate.configuration')
        config = Configuration(1)
        # AccountConfiguration = Pool().get('account.configuration')
        # account_config = AccountConfiguration(1)
        product_list = []
        if config and config.product_leasing and config.product_admin:
            product_admin_id = config.product_admin.id if config.product_admin else None
            if config.product_leasing:
                product_list.append(config.product_leasing.id)
            if config.product_leasing_commercial:
                product_list.append(config.product_leasing_commercial.id)
            if config.product_sale:
                product_list.append(config.product_sale.id)
            for sale in sales:
                for invoice in sale.invoices:
                    if config.leaseholder_authorization and sale.contract_line:
                        invoice.write([invoice], {'authorization': config.leaseholder_authorization.id})
                    if config.lessor_authorization and sale.lessor_contract:
                        invoice.write([invoice], {'authorization': config.lessor_authorization.id})
                    if config.default_observation_invoice:
                        invoice.write([invoice], {'comment': sale.comment, 'invoice_date': sale.sale_date})
                    for line in invoice.lines:
                        line.update_party_revenue(product_list, product_admin_id)

# This file is part of the sale_weight module for Tryton.
# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool, PoolMeta

MONTHS_STRING = ['', 'ENERO', 'FEBRERO', 'MARZO', 'ABRIL', 'MAYO', 'JUNIO', 'JULIO', 'AGOSTO', 'SEPTIEMBRE', 'OCTUBRE', 'NOVIEMBRE', 'DICIEMBRE']


class VoucherLine(metaclass=PoolMeta):
    __name__ = 'account.voucher.line'

    @classmethod
    def create(cls, vals):
        Configuration = Pool().get('real_estate.configuration')
        config = Configuration(1)
        val = config.get_product_account()
        lines = super(VoucherLine, cls).create(vals)
        for line in lines:
            # month_number = line.voucher.date.month
            detail = ''
            if line.move_line and line.move_line.account and line.move_line.account.kind == 'receivable' and line.voucher.voucher_type == 'payment':
                line.amount = line.amount * (-1)
                detail += 'COBROS POR COMISION '
            if line.move_line and hasattr(line.move_line.origin, '__name__') and line.move_line.origin.__name__ == 'account.invoice':
                # detail = 'MES ' + MONTHS_STRING[month_number]
                if val and line.move_line.account in val.keys():
                    detail += ' ' + val[line.move_line.account]
                # detail = line.detail
                line.detail = detail + ' ' + line.move_line.origin.description
            line.save()
        return lines


class Voucher(metaclass=PoolMeta):
    __name__ = 'account.voucher'

    @classmethod
    def create(cls, vals):
        values = super(Voucher, cls).create(vals)
        for val in values:
            if val.date:
                month_number = val.date.month
                val.description = 'COMPROBANTE CORRESPONDIENTE AL MES ' + MONTHS_STRING[month_number]
            val.save()
        return values
